#!/bin/env bash

WORKDIR="./src"
DESTDIR="./dist"
TMPDIR="./.tmp"
TEMPLATE_FILENAME="./template.html"
SOURCE_EXT=".md"
DEST_EXT=".html"
AUTHOR="Michaël Dupont"

if [ ! -d "$DESTDIR" ]
then
  echo "Creating destination folder..."
  mkdir -p "$DESTDIR"
fi

echo "Removing former generated files..."
rm -rf "$DESTDIR"/*
rm -rf "$TMPDIR"  # Just in case it was not completed on last run

if [ ! -d "$TMPDIR" ]
then
  echo "Creating temporary folder..."
  mkdir -p "$TMPDIR"
fi

echo "Generating HTML pages..."
find "$WORKDIR" -type f -iname "*$SOURCE_EXT" | while read SOURCE_FILENAME; do
  BASE_FILENAME=$(basename "$SOURCE_FILENAME" "$SOURCE_EXT")
  DEST_FILENAME="$BASE_FILENAME$DEST_EXT"
  DEST_PATH=${DESTDIR}/${DEST_FILENAME}
  echo "$SOURCE_FILENAME => $DEST_PATH"
  PUBDATE=$((head -3 | tail -1 | cut -d " " -f2 | cut -d "-" -f1-3) < "$SOURCE_FILENAME")
  PUBYEAR=$(echo "$PUBDATE" | cut -d "-" -f1)
  PUBMONTH=$(echo "$PUBDATE" | cut -d "-" -f2)
  PUBDAY=$(echo "$PUBDATE" | cut -d "-" -f3)
  TITLE=$((head -1 | cut -d " " -f2-) < "$SOURCE_FILENAME")

  echo -e "$PUBYEAR\t$PUBMONTH\t$PUBDAY\t$TITLE\t$DEST_FILENAME" >> "$TMPDIR/index.tmp"
  pandoc --lua-filter ./imagetolink.lua --lua-filter ./linkvalidateprotocol.lua -s -f markdown_strict-raw_html+escaped_line_breaks+blank_before_header+space_in_atx_header+blank_before_blockquote+backtick_code_blocks+definition_lists+table_captions+multiline_tables+pandoc_title_block+all_symbols_escapable+intraword_underscores+strikeout+superscript+subscript+footnotes+inline_notes+emoji -t html5 --no-highlight --template="$TEMPLATE_FILENAME" -V author="$AUTHOR" -V lang=en -o "$DEST_PATH" "$SOURCE_FILENAME" &
done

echo "Generating index page..."
echo -e "title: Celui.la\n" > "$TMPDIR/index.md"

sort -r "$TMPDIR/index.tmp" | while read line; do
  YEAR=$(echo "$line" | cut -f1)
  MONTH=$(echo "$line" | cut -f2)
  DAY=$(echo "$line" | cut -f3)
  TITLE=$(echo "$line" | cut -f4)
  URL=$(echo "$line" | cut -f5)
  HEADER="$YEAR-$MONTH-$DAY"
  echo -e "\n<small>\n$HEADER\n</small>  \n[${TITLE}](./${URL})"
done >> "$TMPDIR/index.md"

pandoc -s -f markdown_strict+mmd_title_block -t html5 --template=$TEMPLATE_FILENAME -V hide-title -V is-home -V lang=en -o dist/index.html "$TMPDIR/index.md"
echo "Cleaning temporary files..."
rm -rf "$TMPDIR"
rm -f .~index.tmp .~index.md
echo "Done!"
