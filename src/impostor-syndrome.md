% Impostor syndrome
%
% 2018-12-05

About everyone whose competence has been externally recognized has it.

It's good to repeat it until we have fully internalized it. **About everyone seen as competent feels like an impostor.**

Your skilled colleague. Your over-achiever friend. Your super-sharp psychologist.

When we experience it, we should take it for what it is: **a proof that we are trying to do a good job**. We have this great vision of everything we could do better, and we want to reach it. What we miss is that the mere presence of this drive, this will to do things right, has already put us ahead of the pack.

**Knowing that, we can relax.** If we feel like an impostor, we may think we cannot fail or else we'll be discovered and treated as such. It's bullshit. When we know it's actually a good sign to feel like that, we can try things and be OK with the possibility to fail.

**Nothing bad will happen if you fail.** The only risk is that you may become even better because of what you will have learned by experimenting.
