% Becoming imperfectionist
%
% 2018-11-30

A perfectionist is someone who will spend a lot of energy towards meeting unrealisticly high standards. They take time, and when they deliver it's well thought-out. They learn through a lengthy mental process, by considering everything they can think about.

An imperfectionist is someone who will **intentionally** spend energy towards delivering as soon as it meets lower-than-usual standards. They don't take time, they don't think it through more than strictly necessary, but they deliver much more and learn from feedback, after the thing is already there.

Become an imperfectionist!^[By the way, the title of this post is definitely a nod to Joshua Becker's [Becoming Minimalist](https://www.becomingminimalist.com/)]

**You feel more productive.** You get regular "wins" from delivering something. Since [we build on success](./chasing-imperferction), you're reinforcing your self-esteem every time.

**You learn faster.** We cannot consider everything. We all have our personal blind spots. Producing more means getting more feedback from other people, who have different points of view. They will make you see things you were not able to see by yourself.

**You have a higher probability of actually making something useful for others.** A big part of success is [pure luck](./vizardry). We cannot know in advance what will really work. Quickly testing things out in real conditions is a good way to eventually discover something that other people will like.

**You're happier.** You have no expectations. You won't be disappointed if something doesn't work. You'll be pleasantly surprised when something is received positively. Much less stressful!
