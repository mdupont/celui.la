% Feeling behind, part 1
%
% 2018-11-18

*Where am I in my life? I am too late. I failed. I can see it everywhere around me. I am well over 30 and I am not successful, while this guy is barely 25 and already a star in the field. At his age I didn't even know what to do with my life, and now I missed the opportunity.*

This kind of negative thoughts can cause us a lot of harm, discouraging us from pursuing our passions. The truth is we're getting tricked.

Our brain attaches more importance and gives more attention to what's unusual. What we notice most is not what's representative. We notice the outliers. The anomalies.

Think of someone you know who is popular and outgoing. The person you're thinking about is probably in the top 5% in terms of popularity. They will tend to know much more people than the average. So the other 95% will also surely know someone like them. Only 5% of the population, with almost 100% mindshare when thinking about someone outgoing.

Compare, and you'll despair. You'll always find someone "ahead" in every area. To neutralize it, change the question you ask yourself from *"Where am I in my life?"* to *"Am I progressing?"*.

Where you are now doesn't matter much. No matter you age, start now and more often than not you'll get somewhere satisfying. **The direction of things is more important than their state.**

Stay tuned for [part 2](./feeling-behind-2).
