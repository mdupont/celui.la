% Building a creativity habit
%
% 2018-12-04

I've always admired the ability some people have to constantly come up with new, creative ideas. [Seth Godin's blog](https://seths.blog/), [XKCD webcomic](https://xkcd.com/), [Dilbert](https://dilbert.com/) come to mind. I was asking myself how they manage to have so many ideas, while I seem to struggle to find a couple ones that are worth considering.

I now have the answer. **You can build a habit of being creative.**

We often see [habits](./habits-vs-feats) as being efficient for adopting healthy behaviors, mainly physical activity. Yet habits work for intellectual tasks as well, such as adopting a positive mindset, taking decisions, and being able to generate good ideas.

Starting is a struggle, but **the more you get used to a given mental state, the more it becomes an "always-on" process**, like a second nature. Everything in your environment will fuel the ideas factory you got running at full speed.

It's not that some people have more ideas, they're just more used to it. You too are able to do the same if you want to.
