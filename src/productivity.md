% Getting productive
%
% 2018-11-28

There's this good, haiku-ish quote about productivity: **"One thing at a time. Most important thing first. Start now."** (Caroline Webb, *How to Have a Good Day*).

This one actually helps me when I feel like going through a list of multiple different tasks (it's not every day and [it's OK](./long-term)).

Why is it so helpful?

**One thing at a time.** It's so tempting to do several things at once, switching quickly from one to another. It's so easy to half-do one task because our mind is elsewhere, thinking about all these other tasks. Experiments show that when multitasking, we eventually take more time to deliver with lower quality.

To do something quickly and correctly, it needs our undivided focus. It's also a great way to practice letting go (of other tasks for the moment) and managing frustration (of not doing the other tasks right away).

**Most important thing first.** That's a great way to avoid procrastinating. The most important thing is often neither the most comfortable nor the easiest. But it's the one which will make us feel good about ourselves for having completed it. It is a good practice for executing on what we decide, pushing through discomfort and going after better, long-term gratification rather than short-term soothing.

It may not be easy to decide what is "the most important thing". We may even trick ourselves into spending our time into the comfortable prioritization of tasks rather than making real progress. So do not think too much about it. Make it quick. **Just pick the one that's in your mind right now.** That one, the first one, not one of the alternatives that came to your mind right afterwards. If it's prevalent, it means it's important. "A rather important thing" is good enough, never aim at perfection.

**Start now.** [There is never a right time to start](./crazy-life). There is something you can do right now to make some progress. Forget the idea of completing the task from start to finish in one go. We often don't have the time or energy, and it's not even the most efficient way. What is important is making progress. Do one chunk, what you can from where you are now, and it's enough.

Making this [a habit](./habits-vs-feats) is a great opportunity to remove a lot of stress from our lifes.
