% Is picking a career like going on a quest?
%
% 2019-02-02

Ever since we were little children, we have been asked what career we would like to pursue. The pressure is enormous. In our society, it is considered as one of the biggest decisions in one's life.

On top of the pressure, the narrative in our society is that this career should be a calling, a life mission that is true to us and will fulfill us. We are like a noble knight going on a quest, succeeding against all odds after overcoming obstacles. And the nature of the quest is clear from the start.

However, in our lives, finding a clear quest is hard. If we think we need to find a quest before making a career choice, we may wait for a long time, stuck in [analysis paralysis](https://en.wikipedia.org/wiki/Analysis_paralysis). Some people are lucky in that they know precisely **what** they want to do, so they can work backwards from the goal to find **how** to achieve it. For most of us though, there is no precise **what**, yet we're asked to make a choice so we're trying to find a **how**.

[A solution from Paul Graham](http://www.paulgraham.com/hs.html) is to do the opposite. Ditch the idea that, somewhere, you have a unique perfectly formed calling that will never change and fulfill you for your entire life. Instead of trying to work **backwards** from a goal, work **forward** from promising situations. It may be a special skill you have, a talented friend you can count on, an external opportunity, a degree that opens you more options.

Working forward allows you to be creative, adapt to changes of circumstances, seize new opportunities as they appear, and ultimately gives you a higher chance of success. With less pressure.
