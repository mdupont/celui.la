% It's OK to feel bad
%
% 2018-11-26

I'll repeat it.

*"It's OK to feel bad."*

It is more difficult to realize than it seems. We can get stuck mentally, emotionally, thinking that we should not be feeling bad.

We have **this harmful idea that facing difficulties means we are not enough**. We see other people that are successful and seem to always do the right thing, Of course, one day we realize they're just as much of a mess as us, they're just good at hiding it.

The average person has about 150 items on their mental to-do list at any given time. Many of them will be done late, or forgotten, or just never acted on. That's a normal part of everyone, but most people will hide it.

It's hard to resist the temptation of instant gratification for everyone. Of course, with a vivid imagination we can imagine ourselves as we would be if we always make the right decision for the long term. Logically, we could lose weight in a few months, gain muscles in a few months, save money in a few years...

Well, there's just a catch. It is impossible to consistently behave in an "optimal" way over a long period of time. It just takes too much energy.

So we can relax. We are allowed to feel bad. We are allowed not to do things optimally. It doesn't prevent us from achieving great things.
