% Making a hard decision
%
% 2018-11-14

I have a few tools in my mental toolbox that helped me make a decision when a lot is at stake. When you're confronted to a difficult choice and you find it hard to pick something, try this:

**Deciding is done at the emotional level.** We can make a decision look like it was perfectly rational. It never is. If someone smoke, they usually already know all the factual reasons why it is bad for them. But as long as it's not emotionally painful enough for them, they will keep doing it.

So embrace it. Be in touch with your feelings. What are your core values, the things you would never compromise on? What are your mere wishes, the areas where you have more flexibility? Which choice seems best aligned ?

**Make sure you explore all the options.** We often think of a decision as a binary choice. "Do I choose A or B?". Almost every time though, there are many other options, and you're missing them by framing the decisions as binary. List all the options you can think o. Change the question to "What are my options? Which ones could be a good choice?".

**Pros and cons, enhanced.** A flat list of pros and cons does not take into account the relative importance of each criteria for us. Now you have your list of values and wishes, assign weight to each of them, and take that weights into account when listing the pros and cons of each of the many options.

It is usually very effective at surfacing an option you maybe haven't thought of at first (before brainstorming on options) and that is best aligned with your values.
