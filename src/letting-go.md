% A choice means letting go
%
% 2018-11-29

Life is not about ticking more and more boxes off an imaginary "*perfect life checklist*". Life is about difficult compromises and uncertain tradeoffs. It's about choices.

**Making a choice implies letting go of the other options.** It's not always easy, it's sometimes uncomfortable, and we don't always know if it's the right decision. But we have to choose.

We can trick ourselves into not making a choice, and pondering over all the possibilities *ad infinitum* because at some level, maybe without being aware of it, we don't want to let go of these options. However, this is a choice all by itself, and it will force a decision upon us over time, which most likely will be worse than picking any of these options.

That's the story of the donkey, who is at equal distance of food on one side and water on the other, and is equally hungry and thirsty. Unable to choose, he ends up dead before making a step in any direction.

**Practicing letting go with small choices is the best way not to be afraid to make big choices in the future.** And to be in control of these choices. Your family life, your career, your projects, and eventually your happiness will benefit from intentional choices where you accept to let go of the alternatives you did not pick.
