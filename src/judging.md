% I have everything to be happy
%
% 2018-12-24

As the man was sitting down with his family to enjoy the Christmas dinner they prepared together, he started to feel like an actor playing a role. The mood was cheerful, the meal looked delicious, the room was decorated with taste, as if everything was coming straight out of the idealized image of the perfect family life.

He felt the claws of anxiety clasping his heart. "I am so lucky! I have a nice family, a good job, no big problem. I have everything to be happy. So why am I not?"

He was withdrawing from his joyful surroundings. "What is wrong with me?", he was telling to himself. "Will I always want more and never be able to be satisfied? Am I spoiled, ungrateful, unable to enjoy all the riches I possess?"

He looked at his children. They were happily playing, living in the moment. "Making no judgement on themselves", he thought.

As the realization calmed his thoughts, he started to relax. "Maybe I don't have everything to be happy after all, as the ability to enjoy what I have is all but automatic. I'm lacking it, but I can develop it. I was judging myself for not being happy, without seeing I've never been closer."
