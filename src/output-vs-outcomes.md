% Output vs outcomes
%
% 2018-12-23

Two teams of software engineers compete to build a similar product, a product that could save you a lot of time in your job if well executed.

The first team is led by a charismatic leader with a strong vision. Cash flows in as investors are seduced. Engineers are seduced too. The vision is so compelling they find themselves unable to think of anything better. Their spirits are lifted up and they work harder as a result, to make this vision a reality. There's certainty that the choices are the right ones. They build feature after feature and polish them until they meet expectations. Their output is high.

The other team is full of smart, but uncharismatic, people with no strong leader. Their project is solid so they still attract cash, less than the other team though. They build a shared vision from their multiple points of views. Then they come to you. They ask you your vision of the best product to help you. They integrate it. Then they put the vision to the test. They quickly build a rough version of the feature that seem the most useful, then they hand it over to you to test it. Based on your feedback, they either polish it, change it, or dump it. Their output is lower.

Which team do you think will end up with the most useful software product? How well can we know what other people want? How well can we know what **we** really need? Until we experiment in the real world to find out, until we measure the outcomes, aren't we all guessing?
