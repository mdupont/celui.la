% Undecided
%
% 2018-12-27

We embarked on canoes with a group of friends for our first river descent. A few hours in, there is this 4 meter wide boulder in the middle of the river. To get around it, I must choose between going left or right. There was no clear best option. There was a clear worst option though: hitting the boulder. I focused on what's clear I wanted to avoid, and sure enough my boat went in this very direction.

As soon as my canoe hit the boulder, the current, which was quite strong this day, made it capsize and pushed it under water with everything it contained, myself included. I waited too long to make a decision so the decision was made for me, and I was not enjoying it.

I managed to get around the boulder while preventing the canoe from getting stuck, and I came back to the surface. I got off with a bruise on my shoulder and my ego.

This episode was a great opportunity to learn. I saw there were better strategies than waiting to choose the best solution and focusing on the problem. And I knew I wanted to make my decisions before the decisions are made for me.
