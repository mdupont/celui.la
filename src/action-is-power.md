% Empower yourself with action
%
% 2018-11-09

Sometimes we are waiting for a miracle. We hope to learn a deep, secret truth that was escaping us until now. And we hope it will change everything and give us the better, happier, freer life we wish we had.

Yet at some level, we are aware that we are deceiving ourselves. That we have to do the work. That the deepest truths never are neither secret nor complex. That we already know them very well. We know exactly what we **should** do.

We look at the path and we don't like what we see. It's long, uncomfortable, uncertain, it requires sacrifices and constant efforts. We are not even sure where it leads. It is scary.

So we look elsewhere. Where it's safe. Into the books, the blogs, the podcasts, many of them claiming they found the deep, secret truth we so badly want as an alternative.

Sometimes acquiring knowledge is a way to avoid taking action.

What if we decide to take one step into the path instead? With our imperfect, limited knowledge, with our doubts and frustrations?

Then we will find out that the fear fades away. The second step is much easier. And what we learn on our way is a much more precious knowledge.

We were told for all our lives to think before we act. Now we must learn when to act before we think.
