% You live in the valley, not on top of the peak
%
% 2018-12-10

I recently wrote about the fact that **[we cannot sustain peak energy](./long-term)**, the energy we have during those days where we are incredibly productive and everything seems easy. Then [regression to the mean](https://en.wikipedia.org/wiki/Regression_toward_the_mean) kicks in, and we have normal days where it is not so easy anymore.

**Peak performance is a rat race for perfectionists.** A perfectionist wants to stay up there, on top of the peak. An imperfectionist doesn't care so much about the peak, he's focusing on the valley.

"Peak" is a good word. You climb it for a while, then you descend it and go back where you live and spend most of your time: the valley. Peaks are not meant to be a place where you can live.

To use another analogy, unless you're Spiderman you cannot stick to your ceiling. So better focus on what you're standing upon: the floor.

**Raising your ceiling is just raising something you rarely reach. Raising your floor means raising every step you take.**

Unless you make something so easy or automatic that you will stick to it no matter what is your energy level on a given day, you're just trying to push the ceiling. So think about **valley performance**. What change can you make to become *consistently* better?
