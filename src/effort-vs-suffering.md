% Effort vs suffering - choose your filter
%
% 2020-01-31

Everyone seems to agree that exercising is important. Even a low amount of exercise, regularly, positively impacts our health and morale. Then why some people find it so hard to exercise, while some other face no difficulty whatsoever?

One of the reasons can be found in the different ways different people consider exercising, along with everything else. Everything we experience is interpreted through our own filters, from which we make sense, we create the meaning of this experience for us. 

Our experiences are, by themselves, neutral. **The narratives we slap on top of our experiences is what makes them uplifting or depressing, exciting or frightening for us**. The same feeling can generate wildly different emotions when seen through different filters. Everyone has their own unique set of filters. Even the same person interprets things differently depending on how they feel.

When we're ecstatic, we put on our rose-tinted glasses. Everything we experience is interpreted through a filter of positivity, of joy, and generates intense satisfaction. When we're depressed, we're imprisoned in dark thoughts. Everything we experience is interpreted through a filter of negativity, of sadness, and generates nothing more than dullness.

To start exercising, unless you're addicted to it, requires overcoming some inertia, which may feel like a mild discomfort. If our filter says this discomfort is suffering, we'll tend to avoid it, or we'll quit easily as soon as any difficulty arises. The experience generates a negative feeling.

If, however, our filter considers the mild discomfort as an effort, as progress towards something worthwhile and pleasing, the effort becomes a rewarding experience, one we'll be happy to do again. **By focusing on cultivating everything that makes an experience rewarding, we help it transition to a consistent, pleasing habit**.

Many areas in our lives can benefit from this treatment. What filter are we currently applying on them? Is it one we're happy about? Now we know it's possible to apply other filters, which one would work best?

Filters are the building blocks of the stories we're telling ourselves about ourselves. Eventually, **when you change your narrative, you change your life**.
