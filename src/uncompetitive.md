% Finding the strength to be uncompetitive
%
% 2018-12-19

When running becomes about reaching the lowest time per kilometer among your friends on Strava, when going to the gym becomes about how many reps you did or how much weight you lifted, when your job becomes about your salary or your next promotion, when posting on social media becomes about likes and shares, when attending a course becomes about getting the certificate or degree, you know you've entered **competitive mode**.

Getting competitive about most things seems like a good strategy: there is widespread belief this is a strong, alpha behavior, so you might make a somewhat positive impression on people around you, all the while calming down your insecurities.

But what if there were another approach? **What if you don't need to get competitive?**

You'll run for fun and health without exhausting yourself. You'll go to the gym to enjoy moving your body without hurting it. You'll do the job you care about. You'll post on social media to be creative and express yourself. You'll attend courses for the valuable knowledge they provide you, even if there's no degree.

You'll spare your energy to be competitive where it really matters for you, where you absolutely need to go all the way and be the best. You'll make better choices. You'll be focused, not spread. You'll be secure. You won't need validation from others. You'll be more efficient. And you'll certainly be more relaxed.

There's joy in letting go. **Letting go is not giving up. It's being wise enough to know what matters and what doesn't.**
