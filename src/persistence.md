% It can take years before you see the benefits
%
% 2018-12-13

We crave easy answers to the challenges in our lives. Easy answers offer us a break from the overwhelming complexity of the world. So to better sell, a self-help book or seminar has incentive to promise quick, universal solutions with immense benefits, snackified hacks to deal with every situation, slay your dragons today, save the princess and live happily ever after.

The only catch is that **easy answers rarely work**. A study even postulates [self-help books can make you feel worse about yourself](https://www.sciencedaily.com/releases/2015/11/151117112749.htm). So much for practicing gratitude and mindfulness meditation.

Say I am someone who have read a lot of self-help books. I feel like I should be better equipped than most to live a good, fulfilling life. But I realize I'm still struggling, and reading more books doesn't help. **My books told me I learned great strategies that give me everything to win, so if I don't win it means the problem comes from me.** I am the failure.

So let's get this straight. The problem comes from the books. They often contain worthwhile advice and bits of wisdom, but in order to commercially succeed, they become reluctant to tell you how much **making any lasting change is difficult**. Regardless of the strategy, lasting change requires efforts, focus, persistence, even when you don't see any progress. You have to commit over the long term.

**It can take years before you see the benefits of your efforts**. In the meantime, you'll feel stuck. You'll also feel frustrated because you can only focus your efforts in a couple of areas at once. So you'll be tempted to switch to something else, and lose your progress before their bear fruit.

That's not all. **Anything that doesn't depend on you and only you boils down to luck.** You can improve your odds, but cannot *guarantee* that your relationship will last or that the business you created will be successful. So don't burden yourself if things don't work out despite everything you did.

And of course, self-help is great but you will go faster and further if it's not only yourself. Friends, family, psychologists, coaches, and many others have the potential to group-help you much more.
