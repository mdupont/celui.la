% The trap of comfort
%
% 2018-11-13

Comfort feels so good that we generally have as much of it as we can afford. Nice home, fancy clothes, gourmet food, good looking car, expensive hobbies... When comfort increases, we get used to it very quickly. On the other hand, the mere idea of seeing it decrease can be very stressful.

We can get trapped when **comfort gets in the way of our dreams**. You may have the project to start a business, or change career for something more fulfilling. These are the kind of pursuit that can increase your sense of purpose and long-term happiness. They are also the ones that may significantly lower your income, at least for a time. How can we make it less scary to take the leap?

From what I've seen so far, the main source of fear is what **we often don't know what we really need**. What's the absolute level of comfort under which we cannot feel good enough?

I've found it useful to experiment **doing without** possessions and services I might be afraid to lose. That's one of the basis of the trending minimalist lifestyle. Do without the optional, find out what really adds value for you. The idea is not new at all.

In ancient Greece, the Stoics, often wealthy citizens, had a practice. Every year, spend a few weeks living like the poorest in the city. Dress in the cheapest clothes, eat the cheapest food, sleep in the cheapest places, travel with the cheapest means of transportation. By exposing yourself to these conditions, you learn not to fear them. You learn you can feel happy even in those conditions.

Once fear cannot hold you back, so many options open in front of you.
