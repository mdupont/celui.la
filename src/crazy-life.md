% Life is crazy
%
% 2018-11-17

"*Things are a bit crazy at the moment!*"

"*I'll get to it when things will have calmed down a bit.*"

"*Once this rough patch will be over, I will have time to start this project.*"

How many times have we said or heard these? Did things go along as expected? Wasn't the calm after the storm a calm **before** another storm?

A constant is that **life is always crazy**. Different kinds of crazy, different kinds of difficult, never a smooth sail. That's part of the fun.

So start now. Don't wait. There is no better day than today.
