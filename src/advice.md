% Should you follow your dream?
%
% 2019-01-02

When I was a teenager, I encountered as everyone else the most famous piece of life advice: "Follow your dream", "Do what you're passionate about". It was easy to find in movies, books, articles, interviews of famous people, and about everywhere life advice can be given.

It's bold. It's sexy. It's marketable. Except that, for me at least, it proved to be bad advice.

If good advice can save years of effort by giving you the right strategies, bad advice can make you spend years of efforts in the wrong direction.

Am I *supposed* to have a dream, a passion, a true calling? Is something wrong with me if I have none? Is something wrong with me if I have *too many of them*? How do I know which dream is the true one? How do I know which passion will last for my whole life?

If anything, it left me more confused about what to do with my life. **There were harmful assumptions that I should have one unique dream, somehow already present and fully formed, waiting for me to realize it as I am meant to**.

For someone like the teenage me, highly curious and creative, this is the opposite of what I needed to hear. Everything is interesting. Having dreams, growing new passions is the simple part. I needed resources to help me focus, pick one domain strategically and persist in this direction. I needed to know every endeavor has boring parts, and the necessity to accept, even appreciate the blend of boredom that come with my choice. I needed to know we can get interested in about any domain when we practice and get better at it. I needed practicality over vision.

Now, should I have been a different person, someone who was more practical and having more trouble envisioning where he wants to lead his future, such a piece of advice would have been useful. It would have pushed me towards the vision, dream side of things I'm lacking, and helped me get more effective with my practical skills.

Advice is not universal. **Advice is contextual, useful for a target group of people in a given situation, useless or even harmful to people outside the target group**.

Before following a piece of advice, remember it's OK if it doesn't seem to help you. The problem comes from the advice, not yourself.
