% The counter-intuitive bump
%
% 2020-01-13

When we're always tired, in a constant state of low energy, it becomes unpleasant to even think about exercising. Yet exercising is the very thing that would energize us and bring us into a better state, should we to push through our nolition to exercise.

When it's late but we started watching just one more episode, it was uncontrollably tempting, even if our conscious self was against it and knows it's a bad idea. Yet in just a few minutes the temptation would go away, should we to wait through our urge to binge-watch.

In many of these situations, we have a choice between the easy, intuitive path and the uncomfortable, counter-intuitive path. The first one will feel good in the moment but feel like a failure later, getting you, at best, no better than you were before. The second one requires effort in the moment to break the inertia, but can used as a source of pride and contribute to change your momentum for the better.

**The amount of discomfort you're willing to tolerate for a long-term gain is one of the main indicators of your eventual success.** In life, the bumpy road is always a better choice than the highway.
