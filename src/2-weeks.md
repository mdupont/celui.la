% The 2 weeks threshold
%
% 2018-11-23

2 weeks ago, I started a [daily practice](./daily-practice): publish something on this website, with no restriction or expectation whatsoever. I missed some days, it's OK. With this publication being the 15th, it has been a success so far, and a great learning experience.

Now begins an important turning point. **Good resolutions usually last 2 weeks.** Keep that in mind if you plan on taking resolutions for the upcoming New Year. After 2 weeks the honeymoon period is over, initial motivation is getting lower, our attention is attracted towards other things. It's perfectly normal. The hard part begins.

Between the 2 weeks point and the creation of a real habit, there will be more times when we'll have to **push through**. It's part of the learning experience. Once we have the proof we can push through incomfort and do things anyway, a big mental limitation will have vanished.

At some point, the practice *will* fall apart, even if we have been doing it for many years. It's a normal step of the process. Another reminder that **failure is just a step towards success**. As a buddhist puts it:

> "A Zen teacher told us the way practice works is that we build up our practice, then it falls apart. And then we build it up again, and it falls apart again. This is the way it goes."\
<https://twitter.com/BStulberg/status/1065987349677522944>
