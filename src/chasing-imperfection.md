% Chasing imperfection
%
% 2018-11-12

As a recovering perfectionist, I am practicing very hard doing things quickly. Counter-intuitively, the important part is intentionally delivering something I consider sub-par.

In these posts, less planning, less research linking, less targeting, less tone and grammar review. Basically doing the opposite of what almost any writing / blogging expert will advise you to do.

The benefit is two-fold.

**1. We build on success.** If you want to improve in any area, build a path that has many, frequent small wins along the way. Frustration is counter-productive.

Publishing a blog post, whatever its state, is a success. Working hours and hours on one because of perfectionism is setting ourselves up for failure and frustration.

You may have already seen that idea elsewhere, in the form **systems over goals**, or **processes over outcomes**.

**2. It's useful to be resistant to shame.** Sometimes being ashamed is stopping us from delivering something, from making personal progress, from asking money for a service we provide.

Learning to accept it, accept being incomfortable, may well be what will make the difference over the long term. Something barely working that solves someone's problem is better than something elegant that does not exist yet. And you'll get invaluable feeback and insights by confronting your project to the real world.

Even if you could map things perfectly, the map will never be the territory. To know the territory, explore it. An imperfect map will do.
