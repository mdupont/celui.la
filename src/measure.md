% Measure when necessary
%
% 2018-12-21

We are often repeated that we can only improve what we measure. Fortunately, our options for gathering metrics are almost limitless.

A writing app can tell you how many pages you've written, how many consecutive days you've maintained your writing habit, and if you've met your number of words threshold.

A fitness app with a basic set of devices can tell you precisely what distance you've ran, your pace, the elevation, what was your heart rate, and how it compares with your previous results.

Then comes leaderboards. See how you compare to your friends, or to other community members. See who's still ahead while you're making progress. See who's catching up with you when you're plateauing. See the gap between the progress you've envisioned and your actual progress. Hesitate when you can only do a partial or slower session because it would look bad in your stats. Measure, and feel pressured to perform. Compare, and you'll despair.

That's why the imperfectionist **only measures when it becomes necessary to make progress**. They go out of their way to have no expectations about their progress rate or about their ranking. They just focus on doing the thing as often as possible. Putting the sport shoes on, even without going running, is already something.

You can become a good runner, have fun and reap the health benefits, way before you need any kind of tracking.

**Measuring is helpful when you go professional**. When you know exactly where you need to go and when progress is not straightforward, such as growing a business by analyzing audience feedback. But when you're into something for fun, metrics can become an obstacle.
