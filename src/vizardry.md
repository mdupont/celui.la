% Vizardry? The true power of visualizations
%
% 2018-11-20

In 2006, the book *The Secret* managed to sell 30 million copies by talking about **the law of attraction**. I haven't read it, but since then we regularly see people, including some celebrities, implying that it was a key factor in their success. Did they really dreamt it into reality? Can we transform our lives, become fit and successful with one simple trick?

When people talk about it, they usually go about one of these lines:

- **Visualizations:** *"I visualized what I wanted in a very detailed way, every day for years, and it came true."*
- **Affirmations:** *"I repeated myself, 15 times in a row every day, what I wanted to become, and I succeeded."*
- **Positive thinking:** *"I forced myself to discard any negative thought and only express the positive, and nothing stopped me from achieving my goal."*

Of course, **these practices won't make you successful**.

Because some successful people visualized or made affirmations doesn't mean it was the cause of their success. [Reality is complex](./perspective) and humans are quite bad at analyzing themselves. They cannot know the exact causes of their success, so they find a simple explanation.

Also, these people will be [outliers](./feeling-behind-1). We only know about the successful ones, and nothing about all the ones who practiced the law of attraction and were unsucessful anyways. This is **survivor bias**.

However, I think **these practices can be one useful tool among others in your toolbox**. Here's why.

**1. You need to have a clear goal.** You cannot visualize clearly if you don't really know what you want. And being clear about what you want is an important step towards actually achieving it. Nothing magical, these are your S.M.A.R.T. annual goals at your job right there.

**2. It needs to be really important to you.** If you're able to maintain a daily practice like that over a long period of time, you must really want it. Motivation is helping a lot to achieve something.

**3. It links the goal to your identity.** By putting yourself in the center of your vision or your mantra, you're associating with your goal. Over time, it becomes part of your mental representation of your identity. A person will go to greater lengths to achieve or protect something link to their identity.

**4. It helps you notice the opportunities.** You're clear about your goal, it's always in your mind since you're practicing daily, so you will notice and seize more easily the opportunities that might bring you closer to your goal. Again, nothing magic, we act more about what's in our mind.

**5. Positive thinking gives energy. And more.** Being in a positive state helps you work better, achieve more and think clearer. Other people will like you more and that will create more opportunities for you. Also, it helps for problem solving: from a positive perspective, you may see things you were not able to see from a negative, problem-focused point of view. A warning note: going too far and refusing any negative thought is however dangerous and detrimental to one's mental health.

**6. It helps you to be prepared for success.** We usually think more about the risks, what can go wrong. And sometimes people succeed and are feeling lost because they spent so much time thinking about the risks that they did not think about what to do if they succeed. They can fall down from the very last step. Positive visualization helps you think about your success and be prepared for it.

There is no silver bullet, never. The major factor to success is a good dose of **pure luck**. Born rich and with many well-placed relations? Lucky you. Even then, **hard work** and **skills** are often required.

On top of that, practices like the ones mentioned in this article can **improve your odds**. And that's already a lot.
