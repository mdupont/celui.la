% In motherhood, there's no going back for your body. And it's OK.
%
% 2018-12-19

Mainstream media and social media have an incredibly distorted view on parenthood, especially on motherhood. We already tend to be badly prepared for parenthood, so making good information more difficult to find is not helping. Many new parents end up armed with a mousetrap to face Godzilla.

Pregnancy and motherhood change a woman's body in many ways, and some changes are permanent. Yet what do we hear about the most? How to get your weight back to what it was before pregnancy. Or how to feel feminine again, usually after your partner expressed that he would like to see you as you were before.

I think this line of thinking is viciously promoting the very attitude that is most likely to get you depressed.

First, the media tend to focus on [outliers](./feeling-behind-1), the statistical anomalies who are *not* a sane basis for comparison and will just make you feel worse. That's your favorite influencer fitness-junkie mom who got "back" to her previous weight and a flat stomach 3 months after delivery.

Then the too strong focus on weight may make other, less known changes come as a unexpected downers. What if you never expected to have to let go of your entire collection of shoes because muscle-relaxing hormones during pregnancy permanently made your feet wider? What if your stomach muscles have separated and you have to wait several months before you can start exercising again?

Lastly, the media narrative goes this way: there is a problem, the problem is narrow (weight, femininity), we can solve the problem quickly, the solution is a set of actions, the result is being back to how it was before the problem.

Yet in reality the journey is completely different. There is a whole new body that is not the same as before and needs time to be "adopted". There is no problem, but a new start. There is no narrow domain, it's the whole body. It is not quick nor straightforward, it takes time to get used to it and accept it. There is nothing to solve, this is about adapting. It is not about actions, but mainly about psychological processes. The result is not going back to a previous state, it is finding a new balance.

There is no going back to the former body. And that's OK. There is joy, satisfaction and peace of mind to be found with the new one.
