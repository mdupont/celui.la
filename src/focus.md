% What are you focusing on?
%
% 2018-12-12

We are so good at tricking ourselves. Ask around and listen to what people say. Then look at what they actually do.

The person who expresses wishes for a meaningful job for the greater good, but stays working at a large tobacco firm.

The person who says happiness is their main goal, yet chooses safety every time.

The person who talks about how important family is, and always works overtime.

All these examples are from real persons I've met. I've fell in similar traps myself. Nobody is immune. If you think you are, you are tricking yourself again.

In this age of vast possibilities, too much information, and constant distractions, it is more and more difficult to make a choice and stick to it. **The ability to focus is an increasingly valuable skill.**

We tend to know that our actions are speaking for us, not our wishes, but we don't like it. Wishes are easy and limitless. Actions are limited and often uncomfortable.

**Choosing a course of action forces us to let go of the other possibilities.** It is also the only way to really express ourselves. Making a choice intentionally is great, let's do it before the choice is made for us and rarely in our favor.

**We are what we are focusing on.** What are you focusing on recently? Is it aligned with your wishes and values? If not, this is your opportunity to take the control back.
