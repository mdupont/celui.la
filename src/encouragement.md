% Of remarks and encouragement
%
% 2018-11-26

The way we educate children made incredible progress in the past few decades. We know it's better to support, to positively reinforce through encouragements, to help them verbalize how they feel.

Why don't we do the same with adults? Do we adults work so differently that we cannot apply what is good for children as well?

The simple fact of expressing appreciation or encouragement towards someone else, especially if they're struggling, can make a big difference. We are so used to getting remarks and negative judgments, to hear from others only when they have a complaint, that any positive reinforcement will make our day.

What positivity can we bring to the world today?
