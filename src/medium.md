% A message about Medium
%
% 2018-11-15

When I started hearing about [Medium](https://medium.com), a content publishing platform, a few years ago, it seemed like an interesting service. Now it serves as a cautionary tale about trusting with our data providers we don't control.

At first, they had a clean and pleasant reading experience. They had a powerful text editing interface. And more importantly, they had attracted talented, influential people who wrote interesting articles. It was good marketing.

Media theorist Marshall McLuhan famously said "*The medium is the message*", in that the means by which a message is transmitted influences how a message is perceived.

Medium was the cool, trendy place where smart people were publishing quality articles. The place where readers were taken into account and given a good experience. More and more projects and businesses started to use Medium for their blog. News aggregators often had Medium-hosted articles in their top links.

Then came a small change. The started displaying a full-page popup to coerce visitors into subscribing.

Nowadays, everytime I am about to click on a link to an article published on Medium, I hesitate. I know the popup will come annoy me.

I am not alone, as I see growing dissatisfaction online about the change. What's disappointing is that they were promising a good reading experience. They tout it themselves as one of their selling points. And they broke that promise. They broke one of their first principles.

As a result, the perception of the service is changing. It's becoming another service who did a classic "bait and switch" move to juice out profits, without a thought for their readers.

And as the perception of the medium grows more negative, the perception of the message is affected in the same way. For a good perception, go elsewhere. The medium is the message, even for Medium.
