% Realistic energy level
%
% 2018-11-26

Some days, I have incredible energy and I make fast progress on everything. What was frustrating is that **this state never lasts**. I used to be disappointed with myself, because I wanted to always perform at this level, and I planned things as if it was the case.

Some other days, I have no energy and it becomes incredibly difficult to get anything done. I was getting all the more frustrated because I had this image in mind of my high-performing days. I did not even notice than **this state never lasts either**.

I had short-term focus, seeing only the current days, trying to replicate one day over and over.

Everything changed when somone taught me to look at a broader perspective.

Our mood and energy doesn't work on a day-by-day basis. **It fluctuates over a span of weeks**. That's the way it works.

Now, if you draw a graph of your energy levels over 1 month (or more), you are likely to discover that there are a few days where you had a peak, another few days where you had a valley, and most days where you had a "normal" level.

Your "real" energy level to use as a basis is not a peak or a valley, it's the average over the period. This average will usually be similar to what we don't notice: the "normal" days.

Now I plan according to my real average energy level. I can freely enjoy the peaks without being anxious when they end, and I can freely accept the valleys as normal events without getting frustrated, and just seize the opportunity to relax.
