% Unconcerned about what other people think
%
% 2018-11-29

Learning to feel unconcerned about what other people think is one of the biggest happiness booster I can think of. **You cannot win with peer-pressure**. Most parents know it.

A parent has to follow the society's ideal of individualism. Put their self, their needs first, before the children's. "*You shouldn't feel overwhelmed by child care, take more time for yourself!*"

A parent also has to follow the society's idea of child education. Put their children's needs first, before theirs. "*I see you taking a lot of time for yourself, don't you think you should spend more time with your children?*"

No way to win, many ways to feel like a total failure.

That's one example of the countless ways other people, or the public opinion, can make us waste our [precious, finite energy](./energy). We can use our energy towards worrying about opinions, or we can use it towards doing what is meaningful to us. **The more unconcerned we feel about what other people think, the more energy we have.**

The higher our expectations towards ourselves, or the lower our self-esteem, the more we'll try to fulfill the impossible injunctions from peer-pressure. Among them, **the injunction of performance is the most harmful of all**. The more energy we invest, the more we will impede our progress on meaningful stuff, the less performant we will feel, the lower our self-esteem will become.

How do we escape this trap?

Feeling fully unconcerned about what other people think is a long, difficult endeavour. In a sense, it's a [feat](./habits-vs-feats). The good news is we don't have to do it fully. We don't have to be performant there. That's the magic of **imperfectionism**!

**Any pressure we release from ourselves, even temporarily, is a win.** How do we do it? By practicing of course! We expose ourselves to a risk of mild negative peer-pressure, it should feel a bit uncomfortable. Anxiety will wear off after a while, meaning we've become a bit more resistant. Time to repeat by exposing ourselves a bit more. Every time, we're feeling good about ourselves for making progress, which fuels our self-esteem, which makes us even more resistant.

As an example, I'm currently writing articles in this blog, focusing on publishing daily, not on quality or great topics. I'm certainly imagining negative judgment about them. And practicing this certainly helped me care less.
