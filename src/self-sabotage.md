% Give yourself the permission to succeed
%
% 2018-12-07

You've put a great deal of effort in a project that is meaningful to you.

It may be training for a sports [feat](./habits-vs-feats), such as running a marathon at a predetermined competition.

It may be writing a book and publishing it.

It may be attending courses on top of your day job to get an important degree.

But as you're close to reaching your objective, as you have the finish line in sight, you stumble. Everything starts to collapse.

And yet you've already faced many letdowns along the way, and until now you've always stayed strong. You've always got back on your feet.

But not this time. Your dream slowly slips out of your reach.

As you watch it fade away, you secretly feel relieved. Just a bit, just a moment, before it is repressed.

You were held back by a tiny part of you that believes [you're not entirely worthy of succeeding](./impostor-syndrome), and, always anxious about risks, **prevents you from rising to keep the risk of downfall as low as possible**.

Or maybe it got **scared of actually winning**. All the struggles and failures you went through, they're not scary. It got so used to them that they feel like home, they're [comfortable](./comfortable-trap).

Winning, on the other hand, is the unknown. It doesn't know how to handle it. So **it keeps you in the safe zone of comfy struggles and failures**.

**If a part of you is holding you back, it's to protect you.** It means well. Fighting it would only make it worry even more about you.

Most of the time, all you need is **to give yourself the permission to win**. Say it to yourself, allow it.

Use [visualizations](./vizardry) or any other way to **do "as-if" you're in the future and have already succeeded**, to be prepared for when it will come, and know what to do then.

Use it also to explore **the worst possible way you could fail**, to make sure you are well prepared to avoid these pitfalls and to realize that even if the worst happens, you could handle it. You can have a plan B and a plan C, just in case.

If you've explicitly allowed yourself to succeed, and you have a plan in case of success as well as in case of failure, then this anxious part of you will be reassured, and it won't hold you back anymore.
