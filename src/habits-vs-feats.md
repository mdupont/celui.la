% Habits, not feats
%
% 2018-11-16

If you're looking to make real progress, aim at **creating habits** rather than achieving feats.

Feats usually require a huge effort and produce temporary benefits. Habits are automatic once established and compound over time, capable of producing tremendous long-term benefits.

Take exercising. A feat: training intensively over a few months and complete a marathon run. A habit: run every Tuesday and Thursday at lunch time, no lower limit in time or distance or speed, even walking 50m in your training gear is a win.

How many people will abandon their "feat training" because it takes too much of their time, or because life happens and they have other priorities, or because it is too physically taxing, or because they injure themselves at some point, or because the objective is too far and the get discouraged? They will feel frustrated by the experience and bad about themselves.

**A feat requires making its achievement a priority**, the only way to "win" is to complete it, and we walk a fine line not to push our limits too much.

Even among those who complete the marathon run, some could see the task as "complete" and stop training afterwards, limiting the health benefits over the long term.

On the other hand, if you commit to a habit of running without having any performance requirement, it will be much more comfortable, easy to stick to over the long term. **A habit is light enough on time and energy to allow for life to happen while remaining unchanged**. You "win" every time you stick to it, and remember **[we build on success](./chasing-imperfection)**. Over time, the health and energy benefits will make a big difference.

What about progress in terms of performance then? Well, it's optional. If this is something you want for the long term, seek slow, steady progress. Start at a fraction of the maximum effort you can do, then augment by a tiny amount weekly. If you're able to run 10km, start at no more than 5km, then augment by 3% a week. In a year, you would go from 5km to 23km while staying comfortable.
