% You should be thankful
%
% 2018-12-14

A massively harmful phrase, subtly perverse in its way to make you feel inadequate, ashamed, spoiled. It diminishes you in the disguise of helping you with wisdom. It insinuates you are not allowed to feel what you're currently feeling. Never listen to it.

How you feel is what matters, more than the context in which it happens. **Feelings have an uncontrollable tendency to grow stronger the more we are repressing them**. Let them come and go, they'll pass.

If you're experiencing post-partum depression, it will only make things worse to believe you should be thankful to have a child. All depressive states deserve care and support.

If you're worrying a lot about your career, because societal pressure tells you should have a high status job, but also one that is fulfilling and serves a purpose, all the while being present for your family, then it won't help if you think it's a first-world problem and you should be thankful to even have a choice.

A problem is a problem. Having the choice of career is a privilege, but it's also an opportunity to be in the position to have a positive impact on the world. It's a big responsibility.

Also, we are not autonomous islands. We function in groups, societies, civilizations, all of these have belief systems that influence us without any of us being able to fully escape them. **Feeling societal pressure is normal.** The funny thing is that we think we should be able to escape it as individuals *because* we're living in a society with a radically individualistic belief system.

If you decide to practice gratitude, or focus intentionally on the bright side of things, it can be a healthy practice that elevates you. It's just making more room for positive feelings, while keeping ample space for negative ones as they choose to come. And go. Unrepressed.

Forcing someone to feel a certain way is just the opposite, fueling negative feelings by trying to harm them with alleged positive ones. A wolf is sheep's clothing is still a wolf.

Nobody can tell you how you should feel.
