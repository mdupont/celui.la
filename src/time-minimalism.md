% A time for minimalism
%
% 2019-12-30

There are still **only 24 hours in a day**, and more and more ways to spend them. We are drowned under a sea of possible activities and instantly available entertainment. The stream becomes a torrent. The more options we have, the more anxious we feel, the more precious is our time.

Are you feeling overwhelmed by this abundance of choices? Are you struggling internally to find time for what is important to you? Now is the time to get back in control.

Enter **minimalism**. Maybe you've heard of it. Maybe you're picturing spartan white interiors. Maybe you're thinking about hipstery reverse dick-measuring contest about who will own the fewer possessions. Maybe you have been misled.

**Everyone is a minimalist**, most of the time unintentionally. We all have to minimize some aspects of our life in order to maximize some others, because there are still only 24 hours in a day. The trick is to be conscious about it, and to choose, **intentionally**, what we want to maximize in our life.

At its core, **minimalism is about making space for the things that truly matter**. We may have lost track of what things really matter to us. So it's time to stop, make a pause, reconnect to ourselves and write down what gives us a **deep** sense of satisfaction, of progress, of **accomplishment**. What else, on the other hand, only gives us a **shallow**, fleeting pleasing moment that kind of adds, in the background, to a creeping feeling of **mental fatigue**?

Let's consider how much of our limited free time we spend in the deep zone compared to the shallow zone. Let's **make a conscious decision** to maximize the things that truly matter, and miminize the others. Then let's be surprised by how much our mood improves and our energy level increases. 
