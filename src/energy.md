% Your energy is your most precious resource
%
% 2018-11-27

One day, soon after my first son was born, amidst acute sleep deprivation and general exhaustion, I had an epiphany: **my energy is my most precious resource, it is finite, and I must spend it wisely**.

Until then I was trying to do everything at once, at high level. The things I really need AND the things I merely want AND the things I only like. Despite an earlier burnout, I was under the impression it could be feasible.

That day, I finally understood it was neither feasible nor desirable. And without energy, I couldn't live, only barely survive.

I started to research the ways to **maximize my quality of life while minizing the energy I'm spending**. I started to get rid of the biggest energy sinks, of most of the *likes*, of several of the *wants*. Both physical (clothes, furniture, equipment...) and mental (projects, ideas..) items. Helped by [online resources](https://zenhabits.net/) [about](https://www.becomingminimalist.com/) [minimalist lifestyle](https://www.theminimalists.com/), I cleared space for what really matters.

If [you don't know what you really need](./comfortable-trap), **making space for your needs helps seeing them more clearly**.

You'll find out you don't need much. And that now you know your needs, you are free to focus on them, spending your energy wisely.
