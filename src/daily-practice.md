% Daily practice
%
% 2018-11-22

To do daily practices rights, keep them simple, few and pressure-free.

I used to set ambitious expectations on many daily or regular practices. Exercising every day with at least 3 running sessions a week, reading every day, work on my side projects every day, practice 2 foreign languages every day...

Inevitably, at some point I missed a day. Or a practice. Due to all the pressure I was putting on myself, I felt disappointed. I wasn't long before I abandon them all at once.

Now I have only two daily practices: writing a post on this site, and doing at least 1 push-up. The quality is allowed to be low. The quantity is allowed to be low. Missing days is allowed, no big deal, just continue as usual the next one. What matters is the general **regularity over the long run**. That's the part that will bring you the benefits. [Regularity makes benefits compound](./habits-vs-feats). That's why you should optimize their odds of sticking on the long term.

**Simple:** The simpler a practice is to start, to complete anywhere, anytime, whatever your mood and energy is, the easier it is to achieve. If you need a specific place or piece of equipment, be mindful of what it entails.

**Few:** No rocket science, the more practices you're trying to have at the same time, the higher the chance of not being able to do them all. Even when there are many areas you'd like to practice, it won't work doing them all at once.

**Pressure-free:** Setting up a regular practice is a great way to build a habit, but the habit takes time to fully form. In the meantime, the practice will require more energy from you. The lower your expectations, the more often you'll be able to successfully complete a practice, which will keep you motivated. Do not push yourself, aim at much less than you're able to withstand.

Eventually, these practices, then habits, can become the seed that made an area of your life flourish. Once you've made the tiny, simple first step, it's much easier to continue in the same direction. The tiny writings can make a book start, the lone push-up can begin a healthy lifestyle.
