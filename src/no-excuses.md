% A trick to make a habit stick
%
% 2019-01-05

As the new year begins, many of us are making good resolutions. Losing weight, eating better, exercising... We are feeling motivated to set up new, healthy habits. Yet, by the end of the year, [four out of five people will have given up](https://well.blogs.nytimes.com/2007/12/31/will-your-resolutions-last-to-february/). A third won't even make it past January.

It seems that creating a good habit over the long term is hard. We cannot stay highly motivated for more than a few weeks. How can we make it easier for us?

In my case, I had a great record at not keeping good resolutions. I recently realized that **what was eventually demotivating were the expectations I put on myself** through these resolutions. It created constant pressure.

In the end, either I got injured or sick because the expectations were too high and I pushed too hard, or I found excuses good enough that I was able to skip, lowering the pressure, all the while avoiding being too disappointed at myself for breaking my resolution. After all, I had good excuses, it wasn't my fault.

Excuses can also take the form of self-imposed rules that subtly get in the way: I need to have a snack 30 minutes before an effort or I won't perform good enough, I have to answer all my emails before going to the gym, I need to finish the daily household chores before I can start my writing habit...

With **imperfectionism**, the goal is to have such low expectations that the pressure is barely noticeable and there is no room for excuses. Instead of deciding to work out 45 minutes 5 times a week, I decided to do at least 1 push-up daily. Instead of deciding to write 5 pages a week, I decided to write something daily, even just one word. Whatever my situation, the place I am, the amount of time I have, or my energy level, I am able to meet these expectations. I still skip a day from time to time, and it's no big deal: I just resume the day after.

Once I get started, even for 1 word or 1 push-up, I usually get in the mood for doing more (unless I'm having a truly horrible day). Anything additional feels like a bonus, a stretch goal, so I'm always satisfied for showing up. It is a way of [building on success](./chasing-imperfection). I've already accomplished more in a few months of imperfect habits than with all my previous attempts.

Incidentally, I stumbled upon [a podcast from The Minimalists](https://www.youtube.com/watch?v=_zbyjOQ5NOM) yesterday, talking about a very similar approach from James Clear, that he calls Atomic Habits and wrote a book about it. Which makes me think I'm not the only one for whom it is helpful to adopt an approach with low expectations and leaving no room for excuses.
