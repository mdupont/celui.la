% About
%
% 2018-11-08

This blog is an exploration.

About practicing writing frequently on a variety of topics, almost in a stream-of-consciousness style.

About exploring the minimal form a blog can have while staying comfortable to browse and read.

About exploring what it means to build a website with a low ecological footprint.

About focusing on the content, about being free from marketing, likes, shares, followers, ads and tracking.

This page is not about me.

This blog uses only text, to foster creativity from constraint in the absence of visuals.

Technically, posts are written in a Markdown subset that only supports the most essential text styling, processed by Pandoc, a simple Shell script orchestrates all that and generates static HTML files. The source code can be found at <https://gitlab.com/mdupont/celui.la>.

Some sources of inspiration for this blog are [Dan Luu's blog](https://danluu.com), Barry T. Smith's [motherfuckingwebsite](https://motherfuckingwebsite.com) and [txti](http://txti.es), [Low-tech Magazine](https://solar.lowtechmagazine.com), [SustyWP Wordpress theme](https://sustywp.com/), [Leo Babauta's blog](https://zenhabits.net), [Seth Godin's blog](https://seths.blog) and [Hacker News](https://news.ycombinator.com).

May you enjoy your visit in this space.
