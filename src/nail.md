% Nailing the problem
%
% 2018-12-03

*When all you have is a hammer, everything looks like a nail.*

We've all heard that a thousand times. Yet we're not always listening, and there are areas of our life where we're still falling into the trap. Dealing with difficult situations is one.

**We all have a preferred way of coping with problems.** Getting angry, aggressive. Getting avoidant. Negociating. Shutting off our emotions.

Our favorite strategy is the one that worked for us in important situations in the past. And now everytime we're facing a problem, we tend to use it.

**We sometimes use it so much that we feel like we have no other choice**, even when we are aware of its limitations. Even when we realize that no one behavior is effective in every situation. Even when it ends up hurting us, or people we care about. When the hammer is not the right tool, we may miss and crush our finger.

**Building a toolbox of behaviors is one of the greatest gift we can make ourselves.** It takes time and effort, but it's worth it. This is an area where coaching is a good fit and can save us a lot of time. Once we've grown a diverse set of behaviors that we feel confident to use, we'll have the capacity to pick the right tool for the job.
