% Changing perspective changes everything
%
% 2018-11-11

I learned many useful things while training to become a certified coach. The one, most valuable lesson is that I realized the true, life-changing power of **changing your perspective**.

Reality is incredibly complex. Think about the screen you're looking at, its shape, dimensions, colors, brightness, tiny imperfections, down to individual pixels.

And that's only a infinitesimal part of your physical environment. We cannot apprehend all of reality in all its details, even less so in real time. The truth is, we don't need to. We can survive and reproduce just fine with a brain that makes a lot of approximations and doesn't require too much of precious energy.

As such, to function in the world and make sense of it, every one of us needs to filter the avalanche of sensory input coming at us every instant. And **everyone of us has developed a different set of filters**.

Accepting that fact helps accept other people the way they are, a key to better communication and conflict resolution.

Our own filters can get in our way. Do you feel stuck? Unable to escape a situation? Our filters force us to adopt a specific way of thinking about things. And some filters were useful for us at some point, but are now blocking us.

The solution is changing our perspective.

Do things differently. How often did you find the solution of a problem just by talking about it to someone else? The mere change from thought to talk was enough.

Consult other people, they see things differently, and where you see a wall, they may see an easy way around. We can be the smartest person on earth, we always progress much quicker when someone else is helping us, because they can see what we cannot.

Finally, **use your imagination**. We do have the capability to change our own perspective, just by acting **as if**.

A conflict with someone? Project yourself in the shoes of the other person, think as if you are them, understand things their way.

A challenging situation? Imagine yourself in the future, after you successfully overcame it. Feel as if you're there already. Give advice to your present self.

Anytime you have a challenge, changing your perspective should be part of your toolbox. It helped me many times.
