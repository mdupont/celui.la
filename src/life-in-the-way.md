% Life gets in the way
%
% 2019-05-13

It's easy to do something once. It's easy to perform well and be the best for a short while. The real difficulty is being consistent. The only way to really get better at something is to do it day in, day out. To sustain your focus, whatever happens.

What always happens is that sooner or later, life gets in the way. A lot of stuff suddenly has to be dealt with, all at once. New priorities appear. Tradeoffs have to be made. Your focus weakens. Rough patches have to get gone through. Small tensions accumulate over time and a behavior you thought successful suddenly reveals itself problematic, forcing you to reconsider your approach.

Nobody manages to be perfectly consistent. **There is no failsafe mode.** There is only failing, falling, then getting back on your feet. The best consistency is consistently getting back to what's worthwhile to you, even after months, years or decades of hiatus.
