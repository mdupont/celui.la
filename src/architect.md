% You are the architect of your life
%
% 2018-12-01

**You don't have a "true self"** hidden deep within yourself. You constantly evolve, you are built, shaped by your life's events, on top of a unique set of raw materials that provide you core, but not fixed, traits.

**There is no "true love".** A relation constantly evolves, it is co-built by the 2 partners and it needs constant work to flourish. Maybe it had beautiful foundations that sparked immediate passion between the 2 persons. Maybe it was a slower, more complex process. Both can support an ever-growing creation that gets more and more beautiful with time.

**Nothing is ever perfect and immutable.** We may sometimes wish for an epiphany that will answer all our questions and remove all our doubts, because that would make things so much easier.

The bad news is that's not how things work.

The good news is **we are the architects of our lives** and our relations. It opens tremendous possibilities. We don't have to accept something fixed and predefined. We have the freedom to build something beautiful ourselves.

Of course, we cannot built anything we want. There are some constraints, but less than we usually think. Errors can be made, and things can break down sometimes, but we can rebuild differently. It requires hard work and constant efforts, so it's never done. But it has the ability to make us feel so proud.

A perfect and immutable truth would be much easier if it existed. It would also be boring. Being the architect is much more rewarding.
