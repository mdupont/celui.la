% Intentional decisions
%
% 2019-01-01

Regarding decisions, I strive to make as many of them intentionally. Intentional decisions are driven by what I think and feel is best, based on my experience, my values, my intuition. They are the opposite of forced decisions.

Forced decisions are driven by fear, anxiety, bad habit or addiction. This third part of chocolate cake I know will make me feel bad. This fourth cup of coffee I don't really want but feel pushed to have anyway. This party I find an excuse not to attend, because I'm afraid to feel uncomfortable in a crowd.

Intentional decisions are were I make the right choices, where I become a better person, where I feel proud of myself.

Every decision is a step in the direction we're sending our future self. I want to decide myself where to go.
