% The Walt Disney method to give more chances to your passion project
%
% 2019-08-25

You have a project that is important to you. It can be anything, like making a career choice, starting the business you're dreaming of, writing a book, or embarking in a humanitarian travel around the world. To help make it a reality and ensure its success, I've found that the Walt Disney method is an efficient tool anyone can use.

The Walt Disney method is a brainstorming technique developed by Robert Dilts. While it can be done in groups, it also works very well when done individually. **It consists in simulating a dialogue between 3 types of persons**, 3 personas:

**The Dreamer**

The Dreamer is the visionary, the creative, the charismatic leader, the one who dreams big.

The Dreamer sets to make the impossible possible and gives a direction, a motivation and an ideal to the others. They usually give the original, ambitious idea. For the Dreamer, nothing has limits.

**The Realist**

The Realist is the pragmatic one, the manager, the one who sets up a plan and gets things done.

The Realist takes the vision provided by the Dreamer and finds out how and under which form (even less ambitious) it is feasible to build it with the resources that are available today.

While unable to come up with creative ideas, the Realist prevents the Dreamer from getting too unrealistic, and gives feedback about the real world aspects of the project.

**The Critic**

The Critic is the investigative one, the bringer of common sense, the one who finds flaws and risks in the project or its execution plan, so that they could be made more robust.

It may be that the ambitious idea from the Dreamer is fundamentally empty or conflicts with your values, and the Dreamer should come up with a better one. It may be that the plan overlooked a key time management aspect and the Realist should revise it.

The Critic is always trying to help the project by keeping it from failing.

## In action

For each persona, do as if you were them, play the role and put yourself in their shoes. Let them speak turn by turn.

The creative Dreamer comes up with a "no-limits" idea.

Then the organized Realist comes up with plan to make this idea a reality within the limits of the real world.

Then the eagle-eyed Critic finds risks, flaws, gaps with the plan or the idea itself, and voices them to the Dreamer or the Realist, who will respond with a new idea or a revised plan.

Iterate until all parties are satisfied.

Compared to unstructured thinking about your project, using the Walt Disney method helps ensure you end up with a plan to realize your project, which includes you being prepared to handle the potential difficulties and avoid the potential pitfalls on your way.
