% Feeling behind, part 2
%
% 2018-11-19

In [part 1](./feeling-behind-1), we've seen that comparing ourselves can make us feel like it's too late for us and not even worth trying pursuing our dreams.

We've seen that **direction matters more than destination**. Focus on progressing from where you are, and you can reach a good place.

It remains a bit depressing though, looking in any skill we'd like to be good at, and see people "ahead" of us, better at it. Bolder, more outgoing, smarter, better looking... The list can go on.

What if we look at the situation differently?

Yes, if we approach it in a reductionist manner and take every skill in isolation, we'll find people "ahead".

Now, **skills don't work in isolation**. Any success depends on a **smart combination of skills** that makes it more than the sum of its parts. You have a different set of skills than anyone else, and you can choose the ones you'll develop. Do it your own way, with what is meaningful to you.

We are capable of more than we think. Most of the time, when we see a limit it only exists in our mind.
