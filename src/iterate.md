% Practice beats preparation
%
% 2018-12-11

I am a member of a public speaking club. Speaking in public is a valuable skill to have and a good fear to remove. When I started, I worked hard preparing every speech, not giving it until I felt confident enough. Yes I worked hard, yet soon enough I reached a plateau.

**Being well-prepared is not always the best choice.**

It turns out getting good at public speaking requires getting a lot of details right. The posture. The timing, The tone and pace of the voice. The pauses. The body language. The visual aids, props and other accessories. The vocabulary. Avoiding filler words or hesitations. Using humor wisely. Conveying emotions efficiently. Adapting to the audience reactions. Remembering the key points. Doing all that while managing the anxiety.

Nobody can track and control so many details at once. **Details have to become automatic**, so you can focus on the important parts.

No amount of preparation will help if you end up being so stressed that you forget everything. No amount of rehearsals will save you if you find out the audience is getting bored and you have to change your initial plan on the fly if you want to win them over. **There are lessons you only learn when confronted to the real thing.**

Another guy became a member of the club around the same time as I. He chose an alternative way. Exposing himself as often as possible, even if he's not prepared. Spending his time getting real practice. Experimenting.

In the beginning it was messy, sometimes outright bad. Then he ended up building the automatic behaviors. Getting so used to the anxiety that it went away. Learning to improvise. Determining the audience favorite topics, including the ones they don't know about yet. Getting valuable feedback. Discovering things that you can only discover through practice.

He improved steadily. At some point, investing into more preparation became the right thing to do.

**The real world is too complex for us to be fully prepared before being exposed to it.** Preparation is often the icing on the cake.

**Quantity is often a prerequisite of quality**. If you want to get better at running, run often. The model of your shoes does not matter much. If you want to get better at playing an instrument, play it often. Your knowledge of music history won't come handy until much later.

Get the priorities right. As Thomas Edison allegedly said:

> *Genius is one percent inspiration and ninety-nine percent perspiration.*
