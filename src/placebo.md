% The Placebo effect
%
% 2018-11-08

Recent research on the placebo effect, as [reported by the New York Times](https://www.nytimes.com/2018/11/07/magazine/placebo-effect-medicine.html) offers an unconventional and interesting opportunity for healthcare and medicine to improve.

Of course they observed that the mind can have an impact on the well-being of the body, but most people were already convinced. It is not surprising either the strength of the effect is affected by the presence of a specific gene variant.

However their main finding is that **the act of caring** is central to the healing effect.

It tells something about the need for an embodied, empathic medicine that cannot be left behind with the current trend of automation.

It also tells something about all these alternative medicines that, for each of them, some of your friends will swear it changed their life, but for some others it had absolutely no effect.

The actual specifics of these may have less impact than the trust and care that goes in the relation between the practitioner and the patient. And it that regard these people are not shams.

Reality is never simple nor binary. Science, and all of us, have everything to win by abstaining to judge too quickly.
