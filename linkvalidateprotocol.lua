local function starts_with(str, start)
   return str:sub(1, #start) == start
end
function Link(el)
    local is_allowed = false
    local allowed_protocols = {'http://', 'https://', 'ftp://', 'ftps://', 'mailto:','gopher://', './', '#'}
    for i, protocol in ipairs(allowed_protocols)
    do
        if starts_with(el.target, protocol)
        then
            is_allowed = true
            break
        end
    end
    if not is_allowed
    then
        table.insert(el.content, 1, pandoc.Str("["))
        table.insert(el.content, pandoc.Str("](" .. el.target))
        if #el.title > 0
        then
            table.insert(el.content, pandoc.Str(' "' .. el.title .. '"'))
        end
        table.insert(el.content, pandoc.Str ")")
        return el.content
    end
end
